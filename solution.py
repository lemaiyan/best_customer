import utils


def get_best_customers(transactions_csv_file_path, n):
    """
    Function to get the best customer
    :param transactions_csv_file_path: str to the path of the customer CSV file
    :param n: int the number of results to return
    :return:
    """
    # read data using pandas from the csv
    data = utils.read_csv_file(transactions_csv_file_path)
    unique_customers = utils.get_unique_customer_ids(data)
    # variable to store the longest consecutive period
    longest_consecutive_period = 0
    # variable to store the current consecutive period
    current_consecutive_period = 0
    # variable to store the last transaction date
    last_transaction_date = None
    # dict to track customers and their longest_consecutive_period
    track_repayments = {}
    for customer in unique_customers:
        for rec in data:
            if rec[0] == customer:
                if last_transaction_date is None:
                    last_transaction_date = utils.convert_string_to_date(rec[2])
                else:
                    current_date = utils.convert_string_to_date(rec[2])
                    diff = utils.get_date_difference_in_days(current_date, last_transaction_date)
                    if diff == 1:
                        current_consecutive_period += 1
                        last_transaction_date = current_date
                    else:
                        if current_consecutive_period > longest_consecutive_period:
                            longest_consecutive_period = current_consecutive_period
                        current_consecutive_period = 0
                        last_transaction_date = current_date
        track_repayments[customer] = longest_consecutive_period
    # we sort the track_repayments by repaymets and also alphabetically
    sorted_data = utils.sort_dict_by_value_and_alphabetically(track_repayments)
    # print out the requested number of records
    print('all the results: {}'.format(sorted_data))
    print([rec[0] for rec in sorted_data[:n]])


def main():
    """
    The main entry for the program
    :return:
    """
    csv_file = input("Please enter the path to the customer transaction data csv file:\n")
    no_of_results = input("Please enter the number of results to return:\n")
    get_best_customers(csv_file, int(no_of_results))


if __name__ == '__main__':
    main()
