import unittest
import utils
from datetime import date
import csv


class UtilTests(unittest.TestCase):

    def test_get_date_difference_in_days(self):
        date1 = date(2018, 5, 15)
        date2 = date(2018, 5, 10)
        difference_in_days = utils.get_date_difference_in_days(date1, date2)
        self.assertEqual(difference_in_days, 5)

    def test_sort_dict_by_value_and_alphabetically(self):
        to_sort_dict = {"AC5": 20, "AC3": 21, "AC2": 21, "AC1": 30}
        sorted_list = utils.sort_dict_by_value_and_alphabetically(to_sort_dict)
        expected = [('AC1', 30), ('AC2', 21), ('AC3', 21), ('AC5', 20)]
        self.assertListEqual(sorted_list, expected)

    def test_get_unique_keys_in_a_list_of_dicts(self):

        original_list = utils.read_csv_file('data/transaction_data_1.csv')
        unique_keys = utils.get_unique_customer_ids(original_list)
        expected = ['ACC1', 'ACC2']
        self.assertListEqual(unique_keys, expected)

    def test_read_csv_file(self):
            csv_data = utils.read_csv_file('data/transaction_data_1.csv')
            expected = ['ACC1', '32', '2017-01-02 00:00:00']
            print(csv_data)
            self.assertListEqual(csv_data[0], expected)

    def test_convert_string_to_date(self):
        str_date = '2018-05-10 00:00:00'
        expected = date(2018, 5, 10)
        actual = utils.convert_string_to_date(str_date)
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
