import csv, operator
from datetime import datetime


def get_date_difference_in_days(date1, date2):
    """
    get the difference between two dates in days
    :param date1: date
    :param date2: date
    :return: int
    """
    return(date1 - date2).days


def sort_dict_by_value_and_alphabetically(dict):
    """
    Sorts the a dict by value and alphabetically
    :param dict:
    :return: list
    """
    return sorted(dict.items(), key=lambda x: (-x[1], x[0]), reverse=False)


def get_unique_customer_ids(list):
    """
    Gets unique customer_ids from the csv data we just read
    :param list_of_dicts: list
    :param key: str
    :return: list
    """
    unique_keys = []
    for rec in list:
        customer_id = rec[0]
        if customer_id not in unique_keys:
            # this is for ignoring the  header row
            if customer_id != 'customer_id':
                unique_keys.append(customer_id)
    return unique_keys


def read_csv_file(file):
    """
    Reads the contents of a csv file into a dict
    :param file: String
    :return: List
    """
    # read the data in the CSV file
    csv_file = open(file, 'r')
    file_data = csv.reader(csv_file)
    csv_data = sorted(file_data, key=operator.itemgetter(0,2))
    csv_file.close()
    return csv_data


def convert_string_to_date(date_string):
    """
    Convert String into a date
    :param date_string: str
    :return: date
    """
    date_time = datetime.strptime(date_string, '%Y-%m-%d %H:%M:%S')
    return date_time.date()
