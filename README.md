# Best Customer
The purposes of this is to find a solution for getting the best customers who pay consecutively. See the figure below
that shows the state machine of how the solution will be implemented

![Solution State Diagram](https://bitbucket.org/lemaiyan/best_customer/raw/master/solution_state_diagram.png)


## The Solution
The solution is implemented using `python3`. if you don't have `python 3` you can also run it using `python2` 
but you need to make the following changes to the `solution.py` file and replace the following files in the `main`
function 

```python
    csv_file = input("Please enter the path to the customer transaction data csv file:\n")
    no_of_results = input("Please enter the number of results to return:\n")
```
with

```python
csv_file = raw_input("Please enter the path to the customer transaction data csv file:\n")
no_of_results = raw_input("Please enter the number of results to return:\n")
```

### Runing the Solution
To run the solution please follow the following steps
1. Make sure you have `python 3` or `python 2` installed
2. To run solutions run `python solution` for `python 2` or `python3 solution` for `python 3` and follow the prompts.

### Running Tests
The solution ships with the tests for the util functions to run them, run the command `python tests.py` for `python 2` 
or `python3 tests.py` for `python 3`

## Observations
As for the provided test cases of the problem below are the observations

#### data_file = transaction_data_1.csv and n = 1
The best customer is `['ACC2']` as expected from the test cases

#### data_file = transaction_data_2.csv and n = 2
The best customers are `['ACC1', 'ACC3']`  from the test cases we expected `['ACC1', 'ACC4']` the difference being 
that all the results do tie at 2 as shown below 
```python
[('ACC1', 2), ('ACC2', 2), ('ACC3', 2), ('ACC4', 2)]
```
when arranged alphabetically and we get the top 2 best customers we get `['ACC1', 'ACC3']`. Albeit, `ACC4` is still a 
best customer since they scored 2

#### data_file = transaction_data_3.csv and n = 3
Here we get the best customers as `['ACC143', 'ACC144', 'ACC145']` and the expected are `['ACC143', 'ACC214', 'ACC312']`.
The disparity being that the top clients do tie at 4 as shown below 
```python
 [('ACC143', 4), ('ACC144', 4), ('ACC145', 4), ('ACC146', 4), ('ACC147', 4), ('ACC148', 4), ('ACC149', 4), ('ACC150', 4), ('ACC151', 4), ('ACC152', 4), ('ACC153', 4), ('ACC154', 4), ('ACC155', 4), ('ACC156', 4), ('ACC157', 4), ('ACC158', 4), ('ACC159', 4), ('ACC160', 4), ('ACC161', 4), ('ACC162', 4), ('ACC163', 4), ('ACC164', 4), ('ACC165', 4), ('ACC166', 4), ('ACC167', 4), ('ACC168', 4), ('ACC169', 4), ('ACC170', 4), ('ACC171', 4), ('ACC172', 4), ('ACC173', 4), ('ACC174', 4), ('ACC175', 4), ('ACC176', 4), ('ACC177', 4), ('ACC178', 4), ('ACC179', 4), ('ACC180', 4), ('ACC181', 4), ('ACC182', 4), ('ACC183', 4), ('ACC184', 4), ('ACC185', 4), ('ACC186', 4), ('ACC187', 4), ('ACC188', 4), ('ACC189', 4), ('ACC190', 4), ('ACC191', 4), ('ACC192', 4), ('ACC193', 4), ('ACC194', 4), ('ACC195', 4), ('ACC196', 4), ('ACC197', 4), ('ACC198', 4), ('ACC199', 4), ('ACC200', 4), ('ACC201', 4), ('ACC202', 4), ('ACC203', 4), ('ACC204', 4), ('ACC205', 4), ('ACC206', 4), ('ACC207', 4), ('ACC208', 4), ('ACC209', 4), ('ACC210', 4), ('ACC211', 4), ('ACC212', 4), ('ACC213', 4), ('ACC214', 4), ('ACC215', 4), ('ACC216', 4), ('ACC217', 4), ('ACC218', 4), ('ACC219', 4), ('ACC220', 4), ('ACC221', 4), ('ACC222', 4), ('ACC223', 4), ('ACC224', 4), ('ACC225', 4), ('ACC226', 4), ('ACC227', 4), ('ACC228', 4), ('ACC229', 4), ('ACC230', 4), ('ACC231', 4), ('ACC232', 4), ('ACC233', 4), ('ACC234', 4), ('ACC235', 4), ('ACC236', 4), ('ACC237', 4), ('ACC238', 4), ('ACC239', 4), ('ACC240', 4), ('ACC241', 4), ('ACC242', 4), ('ACC243', 4), ('ACC244', 4), ('ACC245', 4), ('ACC246', 4), ('ACC247', 4), ('ACC248', 4), ('ACC249', 4), ('ACC250', 4), ('ACC251', 4), ('ACC252', 4), ('ACC253', 4), ('ACC254', 4), ('ACC255', 4), ('ACC256', 4), ('ACC257', 4), ('ACC258', 4), ('ACC259', 4), ('ACC260', 4), ('ACC261', 4), ('ACC262', 4), ('ACC263', 4), ('ACC264', 4), ('ACC265', 4), ('ACC266', 4), ('ACC267', 4), ('ACC268', 4), ('ACC269', 4), ('ACC270', 4), ('ACC271', 4), ('ACC272', 4), ('ACC273', 4), ('ACC274', 4), ('ACC275', 4), ('ACC276', 4), ('ACC277', 4), ('ACC278', 4), ('ACC279', 4), ('ACC280', 4), ('ACC281', 4), ('ACC282', 4), ('ACC283', 4), ('ACC284', 4), ('ACC285', 4), ('ACC286', 4), ('ACC287', 4), ('ACC288', 4), ('ACC289', 4), ('ACC290', 4), ('ACC291', 4), ('ACC292', 4), ('ACC293', 4), ('ACC294', 4), ('ACC295', 4), ('ACC296', 4), ('ACC297', 4), ('ACC298', 4), ('ACC299', 4), ('ACC300', 4), ('ACC301', 4), ('ACC302', 4), ('ACC303', 4), ('ACC304', 4), ('ACC305', 4), ('ACC306', 4), ('ACC307', 4), ('ACC308', 4), ('ACC309', 4), ('ACC310', 4), ('ACC311', 4), ('ACC312', 4), ('ACC313', 4), ('ACC314', 4), ('ACC315', 4), ('ACC316', 4), ('ACC317', 4), ('ACC318', 4), ('ACC319', 4), ('ACC320', 4), ('ACC321', 4), ('ACC322', 4), ('ACC323', 4), ('ACC324', 4), ('ACC325', 4), ('ACC326', 4), ('ACC327', 4), ('ACC328', 4), ('ACC329', 4), ('ACC330', 4), ('ACC331', 4), ('ACC332', 4), ('ACC333', 4), ('ACC334', 4), ('ACC335', 4), ('ACC336', 4), ('ACC337', 4), ('ACC338', 4), ('ACC339', 4), ('ACC340', 4), ('ACC341', 4), ('ACC342', 4), ('ACC343', 4), ('ACC344', 4), ('ACC345', 4), ('ACC346', 4), ('ACC347', 4), ('ACC348', 4), ('ACC349', 4), ('ACC350', 4), ('ACC351', 4), ('ACC352', 4), ('ACC353', 4), ('ACC354', 4), ('ACC355', 4), ('ACC356', 4), ('ACC357', 4), ('ACC358', 4), ('ACC359', 4), ('ACC360', 4), ('ACC361', 4), ('ACC362', 4), ('ACC363', 4), ('ACC364', 4), ('ACC365', 4), ('ACC366', 4), ('ACC367', 4), ('ACC368', 4), ('ACC369', 4), ('ACC370', 4), ('ACC371', 4), ('ACC372', 4), ('ACC373', 4), ('ACC374', 4), ('ACC375', 4), ('ACC376', 4), ('ACC377', 4), ('ACC378', 4), ('ACC379', 4), ('ACC380', 4), ('ACC381', 4), ('ACC382', 4), ('ACC383', 4), ('ACC384', 4), ('ACC385', 4), ('ACC386', 4), ('ACC387', 4), ('ACC388', 4), ('ACC389', 4), ('ACC390', 4), ('ACC391', 4), ('ACC392', 4), ('ACC393', 4), ('ACC394', 4), ('ACC395', 4), ('ACC396', 4), ('ACC397', 4), ('ACC398', 4), ('ACC399', 4), ('ACC400', 4), ('ACC401', 4), ('ACC402', 4), ('ACC403', 4), ('ACC404', 4), ('ACC405', 4), ('ACC406', 4), ('ACC407', 4), ('ACC408', 4), ('ACC409', 4), ('ACC410', 4), ('ACC411', 4), ('ACC412', 4), ('ACC413', 4), ('ACC414', 4), ('ACC415', 4), ('ACC416', 4), ('ACC417', 4), ('ACC418', 4), ('ACC419', 4), ('ACC420', 4), ('ACC421', 4), ('ACC422', 4), ('ACC423', 4), ('ACC424', 4), ('ACC425', 4), ('ACC426', 4), ('ACC427', 4), ('ACC428', 4), ('ACC429', 4), ('ACC430', 4), ('ACC431', 4), ('ACC432', 4), ('ACC433', 4), ('ACC434', 4), ('ACC435', 4), ('ACC436', 4), ('ACC437', 4), ('ACC438', 4), ('ACC439', 4), ('ACC440', 4), ('ACC441', 4), ('ACC442', 4), ('ACC443', 4), ('ACC444', 4), ('ACC445', 4), ('ACC446', 4), ('ACC447', 4), ('ACC448', 4), ('ACC449', 4), ('ACC450', 4), ('ACC451', 4), ('ACC452', 4), ('ACC453', 4), ('ACC454', 4), ('ACC455', 4), ('ACC456', 4), ('ACC457', 4), ('ACC458', 4), ('ACC459', 4), ('ACC460', 4), ('ACC461', 4), ('ACC462', 4), ('ACC463', 4), ('ACC464', 4), ('ACC465', 4), ('ACC466', 4), ('ACC467', 4), ('ACC468', 4), ('ACC469', 4), ('ACC470', 4), ('ACC471', 4), ('ACC472', 4), ('ACC473', 4), ('ACC474', 4), ('ACC475', 4), ('ACC476', 4), ('ACC477', 4), ('ACC478', 4), ('ACC479', 4), ('ACC480', 4), ('ACC481', 4), ('ACC482', 4), ('ACC483', 4), ('ACC484', 4), ('ACC485', 4), ('ACC486', 4), ('ACC487', 4), ('ACC488', 4), ('ACC489', 4), ('ACC490', 4), ('ACC491', 4), ('ACC492', 4), ('ACC493', 4), ('ACC494', 4), ('ACC495', 4), ('ACC496', 4), ('ACC497', 4), ('ACC498', 4), ('ACC499', 4), ('ACC121', 3), ('ACC122', 3), ('ACC123', 3), ('ACC124', 3), ('ACC125', 3), ('ACC126', 3), ('ACC127', 3), ('ACC128', 3), ('ACC129', 3), ('ACC130', 3), ('ACC131', 3), ('ACC132', 3), ('ACC133', 3), ('ACC134', 3), ('ACC135', 3), ('ACC136', 3), ('ACC137', 3), ('ACC138', 3), ('ACC139', 3), ('ACC140', 3), ('ACC141', 3), ('ACC142', 3), ('ACC100', 2), ('ACC101', 2), ('ACC102', 2), ('ACC103', 2), ('ACC104', 2), ('ACC105', 2), ('ACC106', 2), ('ACC107', 2), ('ACC108', 2), ('ACC109', 2), ('ACC110', 2), ('ACC111', 2), ('ACC112', 2), ('ACC113', 2), ('ACC114', 2), ('ACC115', 2), ('ACC116', 2), ('ACC117', 2), ('ACC118', 2), ('ACC119', 2), ('ACC120', 2)]
```
when arranged alphabetically and we get the top 2 best customers we get `['ACC143', 'ACC144', 'ACC145']`. Albeit,
`'ACC214', 'ACC312']` are still best customers since they also scored 4
